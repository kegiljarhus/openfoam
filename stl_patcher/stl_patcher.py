"""
Utility to extract a set of faces from an STL file, given certain
constraints such as distance from a point or normal vector direction.
"""

import sys
import getopt
import numpy as np
from numpy.linalg import norm

class Facet:
    def __init__(self, n, v1, v2, v3):
        self.n = n
        self.v1 = v1
        self.v2 = v2
        self.v3 = v3
        self.vs = [v1, v2, v3]

class Constraint:
    pass

class NormalConstraint(Constraint):
    def __init__(self, n, tol=1e-6):
        self.n = np.array(n)
        self.tol = tol

    def fulfilled(self, facet):
        return np.sqrt(norm(facet.n-self.n)) < self.tol

class DistanceConstraint(Constraint):
    def __init__(self, c, d, tol=1e-6):
        self.centre = np.array(c)
        self.distance = np.array(d)
        self.tol = tol

    def fulfilled(self, facet):
        for vertex in facet.vs:  
            if norm(vertex - self.centre) <= self.distance:
                return True
        return False

class Stl:
    def __init__(self):
        self.solids = {}

    def facets(self, name):
        return self.solids[name]

    @classmethod
    def from_file(cls, path):
        obj = cls()
        f = open(path)
        while True:
            s = f.readline().split()
            if not s:
                break
            elif s[0] == 'solid':
                name = s[-1]
                facets = []
                while True:
                    s = f.readline().split()
                    if s[0] == 'endsolid':
                        obj.solids[name] = facets
                        break
                    elif s[0] == 'facet':
                        n = np.array([float(i) for i in s[2:]])
                        f.readline() # Outer loop
                        v1 = np.array([float(i) for i in f.readline().split()[1:]])
                        v2 = np.array([float(i) for i in f.readline().split()[1:]])
                        v3 = np.array([float(i) for i in f.readline().split()[1:]])
                        f.readline() # endloop
                        f.readline() # endfacet
                        facets.append(Facet(n,v1,v2,v3))
                        
        return obj                        

    @classmethod
    def from_facets(cls, facets, name='object'):
        obj = cls()
        obj.solids[name] = facets
        return obj


    def write(self, path):
        f = open(path + '.stl', 'w')
        for name, facets in self.solids.iteritems():
            f.write('solid ' + name + '\n')
            for facet in facets:
                f.write('  facet normal %f %f %f\n'%(facet.n[0],facet.n[1],facet.n[2]))
                f.write('    outer loop\n')
                f.write('      vertex %f %f %f\n'%(facet.v1[0],facet.v1[1],facet.v1[2]))
                f.write('      vertex %f %f %f\n'%(facet.v2[0],facet.v2[1],facet.v2[2]))
                f.write('      vertex %f %f %f\n'%(facet.v3[0],facet.v3[1],facet.v3[2]))
                f.write('    endloop\n')
                f.write('  endfacet\n')
            f.write('endsolid\n')


  
class StlPatcher:
    def __init__(self):
        self.constraints = []

    def add_constraint(self, c):
        self.constraints.append(c)

    def extract_patch(self, facets, mode='all'):
        patch = []
        remainder = []
        nc = len(self.constraints)
        for facet in facets:
            fulfilled = [False]*nc
            for i, constraint in enumerate(self.constraints):
                if constraint.fulfilled(facet):
                    fulfilled[i] = True
                    
            if mode == 'all' and all(fulfilled):
                patch.append(facet)
            elif mode == 'any' and any(fulfilled):
                # Slightly wasteful to check all conditions for "any", but cleaner
                patch.append(facet)
            else:
                remainder.append(facet)

        return patch, remainder

