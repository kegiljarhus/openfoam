"""
In this example, we have a channel with three pipes, and want to
extract the pipe inlet/outlet to three separate stl files.
"""
from stl_patcher import *

s = Stl.from_file('geometry.stl')
facets = s.facets('OpenSCAD_Model')
names = ['outlet','suction1', 'suction2']

diameter = 0.35
zdown = [0,0,-1]
for i, x in enumerate([1.0, 3.0, 5.0]):
    centre = [x,0.0,1.2]

    p = StlPatcher()
    p.add_constraint(NormalConstraint(zdown))
    p.add_constraint(DistanceConstraint(centre, 0.5*diameter))

    patch, remainder = p.extract_patch(facets)

    print 'Extracted %i facets into %s' % (len(patch),names[i])
    Stl.from_facets(patch).write(names[i])

    facets = remainder


Stl.from_facets(remainder).write('walls')
