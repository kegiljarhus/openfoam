# README #

### Various solvers and utilities for OpenFOAM ###

The repo currently contains:

* A solver for tracking multiple passive scalars in a flow field
* An inlet-outlet boundary condition for atmospheric boundary layers
* A utility to extract patches from an STL file